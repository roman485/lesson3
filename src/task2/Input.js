import React from 'react';
import PropTypes from 'prop-types';

export const Input = ({ type, placeholder, value, handler, name, title,
                            contentMaxLength }) => {
    
    let info = (value.length === contentMaxLength)? 
        "Max Length: " + contentMaxLength:"";

    return (
        <label>
            <span>{title}: </span>            
            <input
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={handler}
                name={name} 
            />
            <small> {info}</small>            
        </label>
    )
}

Input.propTypes= {
    type: PropTypes.oneOf(['text', 'password', 'number', 'required']),
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType( [PropTypes.string, PropTypes.any] ),
    handler: PropTypes.func.isRequired,
    contentLength: PropTypes.number, // FIXME: in task - bool
    contentMaxLength: PropTypes.number,
}

