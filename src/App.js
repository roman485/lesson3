import React, {Component} from 'react'
import { Toggler } from './task1/Toggler'
import { TogglerItem } from './task1/TogglerItem'
import { Input } from './task2/Input'
import './App.css';

class App extends Component {

	state = {
    activestateGender: "", 
    activestateLayout: "",
    genderText: ['М', 'Ж'],
    layoutText: ['left', 'center', 'right', 'baseline'],
    contentMaxLength: 10,
    name: "",
    password: "",
    age: "", 
    language: ""
	}
 
  changeToggler = value => e => {
    let text = e.target.innerText;
    if(this.state.genderText.includes(text)) 
      this.setState({ activestateGender: value });
    if(this.state.layoutText.includes(text)) 
      this.setState({ activestateLayout: value });
	}

  changeHandler = e => { 
    let name = e.target.name;
    let value = e.target.value;
    const { contentMaxLength } = this.state; 
    if(value.length <= contentMaxLength)
      this.setState({ [name]: value })
  }

  onSubmit = (e) => {
    e.preventDefault();
    const {activestateGender, activestateLayout, name, password, age, language} = this.state;
    console.log('[form data]');
    console.log('[name]', name );
    console.log('[password]',  password );
    console.log('[activestateGender]', activestateGender);
    console.log('[age]', age);
    console.log('[activestateLayout]', activestateLayout );
    console.log('[language]', language );
   } 

  render() {
    const { activestateGender, activestateLayout, contentMaxLength,
            genderText, layoutText, name, password, age, language
          } = this.state; 

    return (

    <div className="App">

      <h5>Task 1-3</h5>

      <form className="divleft" onSubmit={this.onSubmit}>

      <Input type="text" placeholder="Enter name" value={name} 
             handler={this.changeHandler} title="Имя"
             name="name" contentMaxLength={contentMaxLength} /><br/>

      <Input type="password" placeholder="Enter password" value={password} 
             handler={this.changeHandler} title="Пароль"
             name="password" contentMaxLength={contentMaxLength} />

      <Toggler activestate={activestateGender} action={this.changeToggler} name="Пол ">
          <TogglerItem name="male">{genderText[0]}</TogglerItem>
					<TogglerItem name="female">{genderText[1]}</TogglerItem>
			</Toggler> 

      <Input type="number" placeholder="Enter age" value={age} 
             handler={this.changeHandler} title="Возраст"
             name="age" />

      <Toggler activestate={activestateLayout} action={this.changeToggler} name="Layout ">
          <TogglerItem name="left">{layoutText[0]}</TogglerItem>
					<TogglerItem name="center">{layoutText[1]}</TogglerItem>
					<TogglerItem name="right">{layoutText[2]}</TogglerItem>
					<TogglerItem name="baseline">{layoutText[3]}</TogglerItem>
			</Toggler> 

      <Input type="text" placeholder="Enter language" value={language} 
             handler={this.changeHandler} title="Любимый язык"
             name="language" />

        <p/>
        <button className="toggler__item send"> Send </button>


      </form>
 
	</div>

    )
  }
}


export default App; 
