import React from 'react'
import PropTypes from 'prop-types';

export const Toggler = ({ children, action, activestate, name }) => {

    let activeText = activestate;
    return(
        <div className="toggler">
            <span>{ name }: </span>
            {
                React.Children.count(children) > 0 && (
                    <>
                        {
                            React.Children.map( children, ( childItem ) => {
                                if( React.isValidElement(childItem) ){
                                        if(activeText === '') activeText = childItem.props.name;
                                        return React.cloneElement(
                                        childItem,
                                        {
                                            isactive: childItem.props.name === activeText,
                                            action: action( childItem.props.name )
                                        }
                                    );
                                }
                            })
                        }
                    </>
                )
            }
        </div>
    );
}

Toggler.propTypes = {
    name: PropTypes.string.isRequired,
    action:  PropTypes.func.isRequired,
    activestate: PropTypes.string,
    //children: PropTypes.element   // FIXME: return warning
}

