import React from 'react'

export const TogglerItem = ({ children, action, isactive, name }) => {
    return(
        <button 
            type="button"
            className={isactive ? "toggler__item active" : "toggler__item"}
            onClick={ action }
        >{children || name }</button>
    )
}